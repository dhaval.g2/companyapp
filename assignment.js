var companys = [
    { id: 1, companyname: 'Dhaval', owenername: 'Abhay', address: 'Bhavnagar', city: 'bvn', establishdate: '1212-12-02', date: '1212-12-12' },
    { id: 2, companyname: 'Vishva', owenername: 'Abhay', address: 'Surat', city: 'srt', establishdate: '1212-12-02', date: '1212-12-12' },
    { id: 3, companyname: 'Rajdeep', owenername: 'Abhay', address: 'Ahemdabad', city: 'ahm', establishdate: '1212-12-02', date: '1212-12-12' },
    { id: 4, companyname: 'Ravi', owenername: 'Abhay', address: 'Patan', city: 'ptn', establishdate: '1212-12-02', date: '1212-12-12' }
];


var selectedId = 0;

const copytext = (i) => {
    console.log('Customer Copied :' + companys[i].id);
    // var len = companys.length;
    // var lastId = companys[len - 1].id;
    
    var id = companys[i].id;
    var companyname = companys[i].companyname;
    var owenername = companys[i].owenername;
    var address = companys[i].address;
    var city = companys[i].city;
    var establishdate = companys[i].establishdate;
    var date = companys[i].date;
    companys.push({id,companyname,owenername,address,city,establishdate,date});

    display();
}

const updateCompany = (id) => {
    selectedId = id;
    console.log(selectedId);
    console.log('Updated id :' + id);

    var record = companys.filter((item) => (item.id == id));

    document.getElementById('companyName').value = record[0].companyname;
    document.getElementById('owenerName').value = record[0].owenername;
    document.getElementById('address').value = record[0].address;
    document.getElementById('city').value = record[0].city;
    document.getElementById('establishDate').value = record[0].establishdate;
    document.getElementById('Date').value = record[0].date;
    document.getElementById('addBtn').value = 'Update';
}

const deleteCompany = (id) => {
    console.log('Deleted id :' + id);
    companys = companys.filter((item) => (item.id != id));
    display();
}

const addCompay = () => {
    console.log('add');
    var companyname = document.getElementById('companyName').value;
    var owenername = document.getElementById('owenerName').value;
    var address = document.getElementById('address').value;
    var city = document.getElementById('city').value;
    var establishdate = document.getElementById('establishDate').value;
    var date = document.getElementById('Date').value;

    if (selectedId == 0) { // new Data
        var len = companys.length;
        if (len > 0) {
            var lastId = companys[len - 1].id
            var id = lastId + 1;
        } else {
            var id = 1;
        }

        companys.push({ id, companyname, owenername, address, city, establishdate, date });
    } else {  //update

        var array = companys.filter((item) => (item.id == selectedId));
        console.log(array[0]);
        array[0].companyname = companyname
        array[0].owenername = owenername
        array[0].address = address
        array[0].city = city
        array[0].establishdate = establishdate
        array[0].date = date;

        console.log('' + selectedId)
    }
    display();
    resetForm();
}


const resetForm = () => {
    document.getElementById('companyName').value = '';
    document.getElementById('owenerName').value = '';
    document.getElementById('address').value = '';
    document.getElementById('city').value = '';
    document.getElementById('establishDate').value = '';
    document.getElementById('Date').value = '';
    document.getElementById('addBtn').value = 'Add';
}


const display = () => {
    console.log('Displayed');
    var len = companys.length;

    var list = document.getElementById('result');
    list.innerHTML = '';


    for (let i = 0; i < len; i++) {
        let id = companys[i].id;
        var newRecord = document.createElement('tr');

        var newId = document.createElement('td');
        newId.innerHTML = id;
        newRecord.appendChild(newId);

        var newCopanyname = document.createElement('td');
        newCopanyname.innerHTML = companys[i].companyname;
        newRecord.appendChild(newCopanyname);

        var newOwenername = document.createElement('td');
        newOwenername.innerHTML = companys[i].owenername;
        newRecord.appendChild(newOwenername);

        var newAddress = document.createElement('td');
        newAddress.innerHTML = companys[i].address;
        newRecord.appendChild(newAddress);

        var newCity = document.createElement('td');
        newCity.innerHTML = companys[i].city;
        newRecord.appendChild(newCity);

        var newEdate = document.createElement('td');
        newEdate.innerHTML = companys[i].establishdate;
        newRecord.appendChild(newEdate);

        var newTdate = document.createElement('td');
        newTdate.innerHTML = companys[i].date;
        newRecord.appendChild(newTdate);

        var newEdit = document.createElement('td');
        var newEbtn = document.createElement('button');
        newEbtn.className = 'btn btn-primary'
        newEbtn.innerHTML = 'Edit'
        newEbtn.onclick = function (e) {
            e.preventDefault();
            updateCompany(id);
        }
        newEdit.appendChild(newEbtn);
        newRecord.appendChild(newEdit);


        var newDelete = document.createElement('td');
        var newDbtn = document.createElement('button');
        newDbtn.className = 'btn btn-danger'
        newDbtn.innerHTML = 'Delete'
        newDbtn.onclick = function (e) {
            e.preventDefault();
            // deleteCompany(companys[i].id);
            deleteCompany(id);
        }
        newDelete.appendChild(newDbtn);
        newRecord.appendChild(newDelete);

        var newCopy = document.createElement('td');
        var newCbtn = document.createElement('button');
        newCbtn.className = 'btn btn-info'
        newCbtn.innerHTML = 'Copy'
        newCbtn.onclick = function (e) {
            e.preventDefault();
            copytext(i);
        }
        newCopy.appendChild(newCbtn);
        newRecord.appendChild(newCopy);


        list.appendChild(newRecord);
    }
}